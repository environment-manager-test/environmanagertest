import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { OptionsPanelComponent } from './input-form/options-panel/options-panel.component';
import { InputFormComponent } from './input-form/input-form.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule } from "@angular/forms";
import { MainPaneComponent } from './panes/main-pane/main-pane.component';
import { DetailPaneComponent } from './panes/detail-pane/detail-pane.component';

@NgModule({
  declarations: [
    AppComponent,
    OptionsPanelComponent,
    InputFormComponent,
    MainPaneComponent,
    DetailPaneComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    FontAwesomeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
