import { Component, OnInit } from '@angular/core';
import { NgForm } from "@angular/forms";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { QuestionProviderService } from '../question-provider.service';

@Component({
  selector: 'app-input-form',
  templateUrl: './input-form.component.html',
  styleUrls: ['./input-form.component.scss']
})
export class InputFormComponent implements OnInit {

  faSearch = faSearch;
  parameter: string;

  constructor(private qps: QuestionProviderService) { 
    this.parameter = "qid";
  }

  ngOnInit(): void {
  };

  onSubmit(f: NgForm): void {
    if(f.value.parameter === "qid"){
      this.qps.findById(f.value.argument);
    } else if (f.value.parameter === "title") {
      this.qps.findByTitleWords(f.value.argument);
    } else if (f.value.parameter === "answer") {
      this.qps.findAnswers(f.value.argument);
    };

    f.controls["argument"].reset();
  };
};
