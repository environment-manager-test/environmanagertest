import { Component, OnInit, HostListener, ElementRef } from '@angular/core';
import { faLongArrowAltUp, faLongArrowAltDown } from '@fortawesome/free-solid-svg-icons';
import { QuestionProviderService } from 'src/app/question-provider.service';

@Component({
  selector: 'app-options-panel',
  templateUrl: './options-panel.component.html',
  styleUrls: ['./options-panel.component.scss']
})
export class OptionsPanelComponent implements OnInit {
 
  faLongArrowAltUp = faLongArrowAltUp;
  faLongArrowAltDown = faLongArrowAltDown;

  isOpen: boolean;
  baseList: string[]
  
  @HostListener('document:click', ['$event'])
  clickout(event) {
    if(!this.eRef.nativeElement.contains(event.target)) {
      this.isOpen = false;
    } 
  }

  constructor(private qps: QuestionProviderService, private eRef: ElementRef) { }


  ngOnInit(): void {
    this.isOpen = false;
    this.baseList = ['base'];
  };

  onToggle(){
    this.isOpen = !this.isOpen;
  };

  onChecked(test){
    let ix = this.baseList.indexOf(test);
    ix === -1 
      ? this.baseList.push(test)
      : this.baseList.splice(ix, 1);

    this.qps.updateBaseData(this.baseList);
  };
}
