export interface IQuestion {
    id: string;
    title: string;
    correct: string;
    wrong: string[]
}