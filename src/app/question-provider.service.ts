import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { IQuestion } from './types';

import  base from "../assets/quizres.json";
import  cat145 from "../assets/cat145res.json";
import  cat8 from "../assets/cat8res.json";
import  cat9 from "../assets/cat9res.json";
import  cat10 from "../assets/cat10res.json";

@Injectable({
  providedIn: 'root'
})
export class QuestionProviderService {

  main;
  currentData;
  
  private _list$ = new Subject<IQuestion[]>();
  public readonly list: Observable<IQuestion[]> = this._list$.asObservable();

  private _currItem$ = new Subject<IQuestion>();
  public readonly currItem: Observable<IQuestion> = this._currItem$.asObservable();

  constructor() { 
    this.currentData = [...base];

    this.main = {
      base, cat145, cat8, cat9, cat10
    }
  }

  updateBaseData(list: string[]): void {
      let newBaseData = [];
      for (let i of list){
        newBaseData = newBaseData.concat(this.main[i]);
      }
      
      this.currentData = newBaseData;
  }

  findById(partId: string): void {
    const idRx = RegExp(partId, 'i');

    const questions = [];
    this.currentData.forEach(q => {
        if(idRx.test(q.id)) {
            questions.push({
              id: q.id, 
              qst: q
            });
        };
    });

    
    this._list$.next(questions)
  };

  findByTitleWords(partTitle: string): void {
    const titleRx = RegExp(partTitle, 'i');
    const titles = [];
    this.currentData.forEach(q => {
      if(titleRx.test(q["title"])) {
          titles.push({
              title:q["title"], 
              qst: q
          });
      };
    });

    this._list$.next(titles);
  };

  findAnswers(partAnswer: string): void {
    const answerRx = RegExp(partAnswer, 'i');
    const answers = [];
    this.currentData.forEach(q => {
      let currAnswers = [q["correct"], ...q["wrong"]];
      for(let asw of currAnswers){
        if(answerRx.test(asw)){
          answers.push({
            answer:asw, 
            qst: q
          });
        };
      }
    });

    this._list$.next(answers);
  };

  setCurrItem(item): void {
    this._currItem$.next(item);
  };

  getList(): Observable<IQuestion[]> {
    return this.list;
  };

  getCurrItem(): Observable<IQuestion> {
    return this.currItem;
  };

  clearList(): void {
    this._list$.next(null);
  };

  clearCurrentItem(): void {
    this._currItem$.next(null);
  }
};
