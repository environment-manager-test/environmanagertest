import { Component, OnInit } from '@angular/core';
import { QuestionProviderService } from "../../question-provider.service";
import { IQuestion } from "../../types";

@Component({
  selector: 'app-detail-pane',
  templateUrl: './detail-pane.component.html',
  styleUrls: ['./detail-pane.component.scss']
})
export class DetailPaneComponent implements OnInit {

  details: IQuestion;

  constructor(private qps: QuestionProviderService) { 
    qps.getCurrItem().subscribe(data => this.details = data);
  };

  ngOnInit(): void {
  };
  
  closeDetails(){
    this.details = null;
  };
};
