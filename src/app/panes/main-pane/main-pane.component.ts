import { Component, OnInit } from '@angular/core';
import { QuestionProviderService } from "../../question-provider.service";
import { IQuestion } from "../../types";

@Component({
  selector: 'app-main-pane',
  templateUrl: './main-pane.component.html',
  styleUrls: ['./main-pane.component.scss']
})
export class MainPaneComponent implements OnInit {

  list: IQuestion[];
  currItem: IQuestion;

  constructor(private qps: QuestionProviderService) {
    qps.getList().subscribe(data => this.list = data);
    qps.getCurrItem().subscribe(item => this.currItem = item);
  };

  ngOnInit(): void {
  };

  onSelected(item){
    this.qps.setCurrItem(item.qst);
  };

  clearList(){
    this.qps.clearCurrentItem();
    this.qps.clearList();
  };
};
